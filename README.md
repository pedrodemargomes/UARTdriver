## Para rodar:

mv handlerUART.s trab_uart_testa_rx.c ~/cmips/cMIPS/tests/

export PATH=$PATH:~/cmips/cMIPS/bin

cd ~/cmips/cMIPS/tests

compile.sh trab_uart_testa_rx.c ; cd .. ; run.sh

#### PS:

Mudar o arquivo ~/cmips/cMIPS/serial.inp para os testes não demorarem muito.
