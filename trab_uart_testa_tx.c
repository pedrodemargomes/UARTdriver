#include "cMIPS.h"

#include "uart_defs.h"


#define SPEED 3

#define COUNTING (( SPEED +1)*1000) // wait for end of transmission

extern UARTdriver volatile Ud;

extern int volatile tx_has_started;

int probetx() {
  return Ud.ntx;
}

void Putc(char c) {

  disableInterr();

  Tserial volatile *uart;
  uart = (void *)IO_UART_ADDR; // bottom of UART address range

  Ud.ntx--;
  Ud.tx_q[Ud.tx_tl] = c;
  Ud.tx_tl = (Ud.tx_tl + 1) % Q_SZ;

  if( (tx_has_started == 0) && (Ud.ntx == 14) ) {
    uart->interr.setTX = 1;
  }

  enableInterr();

}

void ioctl(Tcontrol ctrl) {
  Tserial volatile *uart;
  uart = (void *)IO_UART_ADDR; // bottom of UART address range

  uart->ctl = ctrl;
}



int main(void) { // receive a string through the UART serial interface
  Tcontrol ctrl;
  Tserial volatile *uart;
  uart = (void *)IO_UART_ADDR; // bottom of UART address range

  int val;
  int *counter = ( void *) IO_COUNT_ADDR; // counter address

  // reset all UART's signals
  ctrl.ign   = 0;
  ctrl.rts   = 0;      // make RTS=0 to keep RemoteUnit inactive
  ctrl.ign2  = 0;
  ctrl.intTX = 0;
  ctrl.intRX = 0;
  ctrl.speed = SPEED;  // operate at the second highest data rate
  uart->ctl  = ctrl;

  ctrl.ign   = 0;
  ctrl.rts   = 0;
  ctrl.ign2  = 0;
  ctrl.intTX = 1;
  ctrl.intRX = 0;
  ctrl.speed = SPEED;  // operate at the second highest data rate
  uart->ctl  = ctrl;

  Ud.ntx = 16;
  Ud.tx_hd = 0;
  Ud.tx_tl = 0;

  tx_has_started = 0;

  char s[] = "abcde\n\x04";
  int i = -1;

  enableInterr();

  do {
    if(probetx() > 0) {
      i++;
      Putc(s[i]);
    }
  } while(s[i] != EOT);


  startCounter ( COUNTING , 0);
  while(  ( val =( readCounter () & 0x3fffffff )) < COUNTING ) {}

  return val;
}
