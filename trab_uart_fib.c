#include "cMIPS.h"
#include "uart_defs.h"
#include "fib_vet.h"

#define SPEED 4

#define COUNTING (( SPEED +1)*1000) // wait for end of transmission

extern UARTdriver volatile Ud;

extern int volatile tx_has_started;

// ++++ Recepcao +++++

int proberx() {
  return Ud.nrx;
}

char Getc() {
  char c;
  
  disableInterr();

  Ud.nrx--;
  c = Ud.rx_q[Ud.rx_hd];
  Ud.rx_hd = (Ud.rx_hd + 1) % Q_SZ;

  enableInterr();
 
  return c;

}

// ++++++++++++++++

// ++++ Transmissao ++++

int probetx() {
  return Ud.ntx;
}

void Putc(char c) {

  disableInterr();

  Tserial volatile *uart;
  uart = (void *)IO_UART_ADDR; // bottom of UART address range

  Ud.ntx--;
  Ud.tx_q[Ud.tx_tl] = c;
  Ud.tx_tl = (Ud.tx_tl + 1) % Q_SZ;

  if( (tx_has_started == 0) && (Ud.ntx == 14) ) {
    uart->interr.setTX = 1;
  }

  enableInterr();

}

// ++++++++++++++++++++



void ioctl(Tcontrol ctrl) {
  Tserial volatile *uart;
  uart = (void *)IO_UART_ADDR; // bottom of UART address range

  uart->ctl = ctrl;
}

Tstatus iostat() {
  Tserial volatile *uart;
  uart = (void *)IO_UART_ADDR; // bottom of UART address range

  return uart->stat;
}


int fibo(int n) {
  return dat[n];
}


int main(void) { // receive a string through the UART serial interface
  Tcontrol ctrl;

  int val;
  int *counter = ( void *) IO_COUNT_ADDR; // counter address

  // reset all UART's signals
  ctrl.ign   = 0;
  ctrl.rts   = 0;      // make RTS=0 to keep RemoteUnit inactive
  ctrl.ign2  = 0;
  ctrl.intTX = 0;
  ctrl.intRX = 0;
  ctrl.speed = SPEED;  // operate at the second highest data rate
  ioctl(ctrl);

  ctrl.ign   = 0;
  ctrl.rts   = 1;
  ctrl.ign2  = 0;
  ctrl.intTX = 1;
  ctrl.intRX = 1;
  ctrl.speed = SPEED;  // operate at the second highest data rate
  ioctl(ctrl);


  // Inicializa variaveis de transmissao
  Ud.ntx = 16;
  Ud.tx_hd = 0;
  Ud.tx_tl = 0;
  tx_has_started = 0;

  // Inicializa variaveis de recepcao
  Ud.nrx = 0;
  Ud.rx_hd = 0;
  Ud.rx_tl = 0;

  char rxFila[256]; int rxHd = 0; int rxTl = 0;
  char txFila[1024]; int txHd = 0; int txTl = 0;
  char tran[9];

  char c,caux;
  int num,aux,i,j,k,auxTxTl;


  enableInterr();

  do {

    // ++++++++++++++ Recebe string +++++++++++
    i = 1;
    c = 'a';
    while( (c != '\n') && (c != EOT) ) {
      if(proberx() > 0) {
        c = Getc();
        rxFila[rxTl] = c;
        rxTl = (rxTl+1) % 256;
        i = i*16;    
      }
    }
    rxTl = (rxTl-1) % 256;
    // +++++++++++++++++++++++++++++++++++++++

    if(c == EOT) {
      break;
    }

    //  ++++ Converte string para numero +++++
    i = i/256;
    num = 0;
    while(i != 0) {
      num += c2i(rxFila[rxHd])*i;
      rxHd = (rxHd +1) % 1024;
      i = i/16;
    }
    // +++++++++++++++++++++++++++++++++++++++

    aux = num = fibo(num);

    // +++++++ Converte numero em string ++++++
    tran[0] = '\n';
    j = 1;
    while(num != 0) {
      caux = num % 16;
      tran[j] = i2c(caux);
      j++;
      num = num / 16;
    }

    for(i = j ; i < 9 ; i++) {
      tran[i] = i2c(0);
    }

    for(i = 8; i >= 0; i--) {
      txFila[txTl] = tran[i];
      txTl = (txTl+1) % 1024;
    }
    // ++++++++++++++++++++++++++++++++++++++


    // +++++++++ Envia string +++++++++++++++
    while( (txHd != txTl) && (probetx() > 0) ) {  // txFila nao vazia
      Putc(txFila[txHd]);
      txHd = (txHd+1) % 1024;
    }
    // ++++++++++++++++++++++++++++++++++++++

  } while(1);
  txFila[txTl] = EOT;
  txTl = (txTl+1) % 1024;

  // +++++++++ Envia string +++++++++++++++
  while( (txHd != txTl) ) {  // txFila nao vazia
    if(probetx() > 0) {
      Putc(txFila[txHd]);
      txHd = (txHd+1) % 1024;
    }
  }
  // ++++++++++++++++++++++++++++++++++++++

  startCounter ( 4*COUNTING , 0);
  while(  ( val =( readCounter () & 0x3fffffff )) < 4*COUNTING ) {}

  return val;
}
