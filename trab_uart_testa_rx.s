	.file	1 "trab_uart_testa_rx.c"
	.section .mdebug.abi32
	.previous
	.nan	legacy
	.module	fp=32
	.module	nooddspreg
	.text
	.align	2
	.globl	proberx
	.set	nomips16
	.set	nomicromips
	.ent	proberx
	.type	proberx, @function
proberx:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	lui	$2,%hi(Ud)
	addiu	$2,$2,%lo(Ud)
	lw	$2,48($2)
	j	$31
	nop

	.set	macro
	.set	reorder
	.end	proberx
	.size	proberx, .-proberx
	.align	2
	.globl	Getc
	.set	nomips16
	.set	nomicromips
	.ent	Getc
	.type	Getc, @function
Getc:
	.frame	$sp,24,$31		# vars= 0, regs= 2/0, args= 16, gp= 0
	.mask	0x80010000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-24
	sw	$31,20($sp)
	jal	disableInterr
	sw	$16,16($sp)

	lui	$2,%hi(Ud)
	addiu	$4,$2,%lo(Ud)
	lw	$3,48($4)
	nop
	addiu	$3,$3,-1
	sw	$3,48($4)
	lw	$3,%lo(Ud)($2)
	nop
	addu	$3,$3,$4
	lbu	$16,8($3)
	nop
	sll	$16,$16,24
	sra	$16,$16,24
	lw	$2,%lo(Ud)($2)
	nop
	addiu	$3,$2,1
	li	$2,-2147483648			# 0xffffffff80000000
	addiu	$2,$2,15
	and	$2,$3,$2
	bgez	$2,$L5
	lui	$3,%hi(Ud)

	addiu	$2,$2,-1
	li	$3,-16			# 0xfffffffffffffff0
	or	$2,$2,$3
	addiu	$2,$2,1
	lui	$3,%hi(Ud)
$L5:
	sw	$2,%lo(Ud)($3)
	jal	enableInterr
	nop

	move	$2,$16
	lw	$31,20($sp)
	lw	$16,16($sp)
	j	$31
	addiu	$sp,$sp,24

	.set	macro
	.set	reorder
	.end	Getc
	.size	Getc, .-Getc
	.align	2
	.globl	main
	.set	nomips16
	.set	nomicromips
	.ent	main
	.type	main, @function
main:
	.frame	$sp,32,$31		# vars= 0, regs= 4/0, args= 16, gp= 0
	.mask	0x80070000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	sw	$31,28($sp)
	sw	$18,24($sp)
	sw	$17,20($sp)
	sw	$16,16($sp)
	li	$2,2			# 0x2
	li	$3,1006632960			# 0x3c000000
	sw	$2,224($3)
	andi	$2,$2,0xff
	ori	$2,$2,0x80
	li	$4,-97			# 0xffffffffffffff9f
	and	$2,$2,$4
	li	$4,-17			# 0xffffffffffffffef
	and	$2,$2,$4
	ori	$2,$2,0x8
	li	$4,-8			# 0xfffffffffffffff8
	and	$2,$2,$4
	ori	$2,$2,0x2
	sw	$2,224($3)
	lui	$3,%hi(Ud)
	addiu	$2,$3,%lo(Ud)
	sw	$0,48($2)
	sw	$0,%lo(Ud)($3)
	sw	$0,4($2)
	jal	enableInterr
	li	$16,97			# 0x61

	lui	$18,%hi(Ud)
	li	$17,4			# 0x4
	addiu	$2,$18,%lo(Ud)
$L11:
	lw	$2,48($2)
	nop
	blez	$2,$L7
	nop

	jal	Getc
	nop

	move	$16,$2
	jal	to_stdout
	move	$4,$2

$L7:
	bne	$16,$17,$L11
	addiu	$2,$18,%lo(Ud)

	move	$2,$0
	lw	$31,28($sp)
	lw	$18,24($sp)
	lw	$17,20($sp)
	lw	$16,16($sp)
	j	$31
	addiu	$sp,$sp,32

	.set	macro
	.set	reorder
	.end	main
	.size	main, .-main
	.ident	"GCC: (GNU) 5.1.0"
