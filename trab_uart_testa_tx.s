	.file	1 "trab_uart_testa_tx.c"
	.section .mdebug.abi32
	.previous
	.nan	legacy
	.module	fp=32
	.module	nooddspreg
	.text
	.align	2
	.globl	probetx
	.set	nomips16
	.set	nomicromips
	.ent	probetx
	.type	probetx, @function
probetx:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	lui	$2,%hi(Ud)
	addiu	$2,$2,%lo(Ud)
	lw	$2,52($2)
	j	$31
	nop

	.set	macro
	.set	reorder
	.end	probetx
	.size	probetx, .-probetx
	.align	2
	.globl	Putc
	.set	nomips16
	.set	nomicromips
	.ent	Putc
	.type	Putc, @function
Putc:
	.frame	$sp,24,$31		# vars= 0, regs= 2/0, args= 16, gp= 0
	.mask	0x80010000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-24
	sw	$31,20($sp)
	sw	$16,16($sp)
	sll	$16,$4,24
	jal	disableInterr
	sra	$16,$16,24

	lui	$2,%hi(Ud)
	addiu	$2,$2,%lo(Ud)
	lw	$3,52($2)
	nop
	addiu	$3,$3,-1
	sw	$3,52($2)
	lw	$3,28($2)
	nop
	addu	$3,$3,$2
	sb	$16,32($3)
	lw	$2,28($2)
	nop
	addiu	$3,$2,1
	li	$2,-2147483648			# 0xffffffff80000000
	addiu	$2,$2,15
	and	$2,$3,$2
	bgez	$2,$L6
	lui	$3,%hi(Ud)

	addiu	$2,$2,-1
	li	$3,-16			# 0xfffffffffffffff0
	or	$2,$2,$3
	addiu	$2,$2,1
	lui	$3,%hi(Ud)
$L6:
	addiu	$3,$3,%lo(Ud)
	sw	$2,28($3)
	lui	$2,%hi(tx_has_started)
	lw	$2,%lo(tx_has_started)($2)
	nop
	bne	$2,$0,$L4
	nop

	lw	$3,52($3)
	li	$2,14			# 0xe
	bne	$3,$2,$L4
	li	$2,1006632960			# 0x3c000000

	lw	$3,232($2)
	nop
	ori	$3,$3,0x40
	sw	$3,232($2)
$L4:
	jal	enableInterr
	nop

	lw	$31,20($sp)
	lw	$16,16($sp)
	j	$31
	addiu	$sp,$sp,24

	.set	macro
	.set	reorder
	.end	Putc
	.size	Putc, .-Putc
	.align	2
	.globl	ioctl
	.set	nomips16
	.set	nomicromips
	.ent	ioctl
	.type	ioctl, @function
ioctl:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	li	$2,1006632960			# 0x3c000000
	sw	$4,224($2)
	j	$31
	nop

	.set	macro
	.set	reorder
	.end	ioctl
	.size	ioctl, .-ioctl
	.section	.rodata.str1.4,"aMS",@progbits,1
	.align	2
$LC0:
	.ascii	"abcde\012\004\000"
	.text
	.align	2
	.globl	main
	.set	nomips16
	.set	nomicromips
	.ent	main
	.type	main, @function
main:
	.frame	$sp,40,$31		# vars= 8, regs= 4/0, args= 16, gp= 0
	.mask	0x80070000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-40
	sw	$31,36($sp)
	sw	$18,32($sp)
	sw	$17,28($sp)
	sw	$16,24($sp)
	li	$2,3			# 0x3
	li	$3,1006632960			# 0x3c000000
	sw	$2,224($3)
	andi	$2,$2,0x1f
	ori	$2,$2,0x10
	li	$4,-9			# 0xfffffffffffffff7
	and	$2,$2,$4
	li	$4,-8			# 0xfffffffffffffff8
	and	$2,$2,$4
	ori	$2,$2,0x3
	sw	$2,224($3)
	lui	$2,%hi(Ud)
	addiu	$2,$2,%lo(Ud)
	li	$3,16			# 0x10
	sw	$3,52($2)
	sw	$0,24($2)
	sw	$0,28($2)
	lui	$2,%hi(tx_has_started)
	sw	$0,%lo(tx_has_started)($2)
	lui	$2,%hi($LC0)
	lw	$3,%lo($LC0)($2)
	addiu	$2,$2,%lo($LC0)
	lw	$2,4($2)
	sw	$3,16($sp)
	jal	enableInterr
	sw	$2,20($sp)

	li	$16,-1			# 0xffffffffffffffff
	lui	$18,%hi(Ud)
	li	$17,4			# 0x4
	addiu	$2,$18,%lo(Ud)
$L15:
	lw	$2,52($2)
	nop
	blez	$2,$L9
	addiu	$2,$sp,16

	addiu	$16,$16,1
	addu	$2,$2,$16
	lb	$4,0($2)
	jal	Putc
	nop

$L9:
	addiu	$2,$sp,16
	addu	$2,$2,$16
	lb	$2,0($2)
	nop
	bne	$2,$17,$L15
	addiu	$2,$18,%lo(Ud)

	move	$5,$0
	jal	startCounter
	li	$4,4000			# 0xfa0

	li	$16,1073676288			# 0x3fff0000
	ori	$16,$16,0xffff
$L11:
	jal	readCounter
	nop

	and	$2,$2,$16
	slt	$3,$2,4000
	bne	$3,$0,$L11
	nop

	lw	$31,36($sp)
	lw	$18,32($sp)
	lw	$17,28($sp)
	lw	$16,24($sp)
	j	$31
	addiu	$sp,$sp,40

	.set	macro
	.set	reorder
	.end	main
	.size	main, .-main
	.ident	"GCC: (GNU) 5.1.0"
