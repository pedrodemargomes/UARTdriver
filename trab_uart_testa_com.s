	.file	1 "trab_uart_testa_com.c"
	.section .mdebug.abi32
	.previous
	.nan	legacy
	.module	fp=32
	.module	nooddspreg
	.text
	.align	2
	.globl	proberx
	.set	nomips16
	.set	nomicromips
	.ent	proberx
	.type	proberx, @function
proberx:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	lui	$2,%hi(Ud)
	addiu	$2,$2,%lo(Ud)
	lw	$2,48($2)
	j	$31
	nop

	.set	macro
	.set	reorder
	.end	proberx
	.size	proberx, .-proberx
	.align	2
	.globl	Getc
	.set	nomips16
	.set	nomicromips
	.ent	Getc
	.type	Getc, @function
Getc:
	.frame	$sp,24,$31		# vars= 0, regs= 2/0, args= 16, gp= 0
	.mask	0x80010000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-24
	sw	$31,20($sp)
	jal	disableInterr
	sw	$16,16($sp)

	lui	$2,%hi(Ud)
	addiu	$4,$2,%lo(Ud)
	lw	$3,48($4)
	nop
	addiu	$3,$3,-1
	sw	$3,48($4)
	lw	$3,%lo(Ud)($2)
	nop
	addu	$3,$3,$4
	lbu	$16,8($3)
	nop
	sll	$16,$16,24
	sra	$16,$16,24
	lw	$2,%lo(Ud)($2)
	nop
	addiu	$3,$2,1
	li	$2,-2147483648			# 0xffffffff80000000
	addiu	$2,$2,15
	and	$2,$3,$2
	bgez	$2,$L5
	lui	$3,%hi(Ud)

	addiu	$2,$2,-1
	li	$3,-16			# 0xfffffffffffffff0
	or	$2,$2,$3
	addiu	$2,$2,1
	lui	$3,%hi(Ud)
$L5:
	sw	$2,%lo(Ud)($3)
	jal	enableInterr
	nop

	move	$2,$16
	lw	$31,20($sp)
	lw	$16,16($sp)
	j	$31
	addiu	$sp,$sp,24

	.set	macro
	.set	reorder
	.end	Getc
	.size	Getc, .-Getc
	.align	2
	.globl	probetx
	.set	nomips16
	.set	nomicromips
	.ent	probetx
	.type	probetx, @function
probetx:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	lui	$2,%hi(Ud)
	addiu	$2,$2,%lo(Ud)
	lw	$2,52($2)
	j	$31
	nop

	.set	macro
	.set	reorder
	.end	probetx
	.size	probetx, .-probetx
	.align	2
	.globl	Putc
	.set	nomips16
	.set	nomicromips
	.ent	Putc
	.type	Putc, @function
Putc:
	.frame	$sp,24,$31		# vars= 0, regs= 2/0, args= 16, gp= 0
	.mask	0x80010000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-24
	sw	$31,20($sp)
	sw	$16,16($sp)
	sll	$16,$4,24
	jal	disableInterr
	sra	$16,$16,24

	lui	$2,%hi(Ud)
	addiu	$2,$2,%lo(Ud)
	lw	$3,52($2)
	nop
	addiu	$3,$3,-1
	sw	$3,52($2)
	lw	$3,28($2)
	nop
	addu	$3,$3,$2
	sb	$16,32($3)
	lw	$2,28($2)
	nop
	addiu	$3,$2,1
	li	$2,-2147483648			# 0xffffffff80000000
	addiu	$2,$2,15
	and	$2,$3,$2
	bgez	$2,$L11
	lui	$3,%hi(Ud)

	addiu	$2,$2,-1
	li	$3,-16			# 0xfffffffffffffff0
	or	$2,$2,$3
	addiu	$2,$2,1
	lui	$3,%hi(Ud)
$L11:
	addiu	$3,$3,%lo(Ud)
	sw	$2,28($3)
	lui	$2,%hi(tx_has_started)
	lw	$2,%lo(tx_has_started)($2)
	nop
	bne	$2,$0,$L9
	nop

	lw	$3,52($3)
	li	$2,14			# 0xe
	bne	$3,$2,$L9
	li	$2,1006632960			# 0x3c000000

	lw	$3,232($2)
	nop
	ori	$3,$3,0x40
	sw	$3,232($2)
$L9:
	jal	enableInterr
	nop

	lw	$31,20($sp)
	lw	$16,16($sp)
	j	$31
	addiu	$sp,$sp,24

	.set	macro
	.set	reorder
	.end	Putc
	.size	Putc, .-Putc
	.align	2
	.globl	ioctl
	.set	nomips16
	.set	nomicromips
	.ent	ioctl
	.type	ioctl, @function
ioctl:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	li	$2,1006632960			# 0x3c000000
	sw	$4,224($2)
	j	$31
	nop

	.set	macro
	.set	reorder
	.end	ioctl
	.size	ioctl, .-ioctl
	.align	2
	.globl	iostat
	.set	nomips16
	.set	nomicromips
	.ent	iostat
	.type	iostat, @function
iostat:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	move	$2,$4
	li	$3,1006632960			# 0x3c000000
	addiu	$3,$3,224
	lw	$3,4($3)
	j	$31
	sw	$3,0($4)

	.set	macro
	.set	reorder
	.end	iostat
	.size	iostat, .-iostat
	.align	2
	.globl	fibo
	.set	nomips16
	.set	nomicromips
	.ent	fibo
	.type	fibo, @function
fibo:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	sll	$4,$4,2
	lui	$2,%hi(buf)
	addiu	$2,$2,%lo(buf)
	addu	$4,$4,$2
	lw	$2,0($4)
	j	$31
	nop

	.set	macro
	.set	reorder
	.end	fibo
	.size	fibo, .-fibo
	.align	2
	.globl	main
	.set	nomips16
	.set	nomicromips
	.ent	main
	.type	main, @function
main:
	.frame	$sp,1336,$31		# vars= 1280, regs= 10/0, args= 16, gp= 0
	.mask	0xc0ff0000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-1336
	sw	$31,1332($sp)
	sw	$fp,1328($sp)
	sw	$23,1324($sp)
	sw	$22,1320($sp)
	sw	$21,1316($sp)
	sw	$20,1312($sp)
	sw	$19,1308($sp)
	sw	$18,1304($sp)
	sw	$17,1300($sp)
	sw	$16,1296($sp)
	li	$2,1006632960			# 0x3c000000
	li	$3,4			# 0x4
	sw	$3,224($2)
	li	$3,156			# 0x9c
	sw	$3,224($2)
	lui	$3,%hi(Ud)
	addiu	$2,$3,%lo(Ud)
	li	$4,16			# 0x10
	sw	$4,52($2)
	sw	$0,24($2)
	sw	$0,28($2)
	lui	$4,%hi(tx_has_started)
	sw	$0,%lo(tx_has_started)($4)
	sw	$0,48($2)
	sw	$0,%lo(Ud)($3)
	sw	$0,4($2)
	jal	enableInterr
	move	$20,$0

	move	$19,$0
	move	$16,$0
	lui	$18,%hi(Ud)
	li	$21,-2147483648			# 0xffffffff80000000
	addiu	$22,$21,255
	addiu	$21,$21,1023
	li	$fp,-256			# 0xffffffffffffff00
$L41:
	b	$L16
	li	$17,10			# 0xa

$L19:
	jal	Getc
	nop

	move	$23,$2
	addiu	$2,$sp,16
	addu	$2,$2,$19
	addiu	$19,$19,1
	and	$19,$19,$22
	bgez	$19,$L17
	sb	$23,0($2)

	addiu	$19,$19,-1
	or	$19,$19,$fp
	addiu	$19,$19,1
$L17:
	beq	$23,$17,$L18
	li	$2,4			# 0x4

	beq	$23,$2,$L18
	nop

$L16:
	addiu	$2,$18,%lo(Ud)
	lw	$2,48($2)
	nop
	bgtz	$2,$L19
	addiu	$2,$18,%lo(Ud)

	b	$L39
	nop

$L18:
	bne	$16,$19,$L21
	move	$17,$20

	b	$L22
	move	$16,$19

$L20:
$L39:
	lw	$2,48($2)
	nop
	blez	$2,$L20
	addiu	$2,$18,%lo(Ud)

	b	$L19
	nop

$L21:
	li	$5,-1024			# 0xfffffffffffffc00
	li	$4,-256			# 0xffffffffffffff00
$L33:
	addiu	$2,$sp,16
	addu	$2,$2,$17
	addiu	$3,$sp,16
	addu	$3,$3,$16
	lbu	$3,0($3)
	addiu	$17,$17,1
	and	$17,$17,$21
	bgez	$17,$L23
	sb	$3,256($2)

	addiu	$17,$17,-1
	or	$17,$17,$5
	addiu	$17,$17,1
$L23:
	addiu	$2,$16,1
	and	$2,$2,$22
	bgez	$2,$L24
	nop

	addiu	$2,$2,-1
	or	$2,$2,$4
	addiu	$2,$2,1
$L24:
	bne	$19,$2,$L33
	move	$16,$2

	b	$L25
	li	$fp,-1024			# 0xfffffffffffffc00

$L26:
$L40:
	lw	$2,52($2)
	nop
	blez	$2,$L26
	addiu	$2,$18,%lo(Ud)

$L28:
	addiu	$2,$sp,16
	addu	$2,$2,$20
	lb	$4,256($2)
	jal	Putc
	addiu	$20,$20,1

	and	$20,$20,$21
	bgez	$20,$L25
	nop

	addiu	$20,$20,-1
	or	$20,$20,$fp
	addiu	$20,$20,1
$L25:
	beq	$20,$17,$L32
	addiu	$2,$18,%lo(Ud)

	lw	$2,52($2)
	nop
	bgtz	$2,$L28
	addiu	$2,$18,%lo(Ud)

	b	$L40
	nop

$L32:
	move	$20,$17
$L22:
	li	$2,4			# 0x4
	bne	$23,$2,$L41
	li	$fp,-256			# 0xffffffffffffff00

	move	$5,$0
	jal	startCounter
	li	$4,20000			# 0x4e20

	li	$16,1073676288			# 0x3fff0000
	ori	$16,$16,0xffff
$L30:
	jal	readCounter
	nop

	and	$2,$2,$16
	slt	$3,$2,20000
	bne	$3,$0,$L30
	nop

	lw	$31,1332($sp)
	lw	$fp,1328($sp)
	lw	$23,1324($sp)
	lw	$22,1320($sp)
	lw	$21,1316($sp)
	lw	$20,1312($sp)
	lw	$19,1308($sp)
	lw	$18,1304($sp)
	lw	$17,1300($sp)
	lw	$16,1296($sp)
	j	$31
	addiu	$sp,$sp,1336

	.set	macro
	.set	reorder
	.end	main
	.size	main, .-main
	.globl	buf
	.data
	.align	2
	.type	buf, @object
	.size	buf, 180
buf:
	.word	1
	.word	1
	.word	2
	.word	3
	.word	5
	.word	8
	.word	13
	.word	21
	.word	34
	.word	55
	.word	89
	.word	144
	.word	233
	.word	377
	.word	610
	.word	987
	.word	1597
	.word	2584
	.word	4181
	.word	6765
	.word	10946
	.word	17711
	.word	28657
	.word	46368
	.word	75025
	.word	121393
	.word	196418
	.word	317811
	.word	514229
	.word	832040
	.word	1346269
	.word	2178309
	.word	3524578
	.word	5702887
	.word	9227465
	.word	14930352
	.word	24157817
	.word	39088169
	.word	63245986
	.word	102334155
	.word	165580141
	.word	267914296
	.word	433494437
	.word	701408733
	.space	4
	.ident	"GCC: (GNU) 5.1.0"
