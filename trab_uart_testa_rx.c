#include "cMIPS.h"

#include "uart_defs.h"


#define SPEED 2

extern UARTdriver volatile Ud;

int proberx() {
  return Ud.nrx;
}

char Getc() {
  char c;
  
  disableInterr();

  Ud.nrx--;
  c = Ud.rx_q[Ud.rx_hd];
  Ud.rx_hd = (Ud.rx_hd + 1) % Q_SZ;

  enableInterr();
 
  return c;

}



int main(void) { // receive a string through the UART serial interface
  Tcontrol ctrl;
  Tserial volatile *uart;  // tell GCC not to optimize away code
  uart = (void *)IO_UART_ADDR; // bottom of UART address range

  // reset all UART's signals
  ctrl.ign   = 0;
  ctrl.rts   = 0;      // make RTS=0 to keep RemoteUnit inactive
  ctrl.ign2  = 0;
  ctrl.intTX = 0;
  ctrl.intRX = 0;
  ctrl.speed = SPEED;  // operate at the second highest data rate
  uart->ctl  = ctrl;

  ctrl.ign   = 0;
  ctrl.rts   = 1;      // make RTS=1 to activate RemoteUnit
  ctrl.ign2  = 0;
  ctrl.intTX = 0;
  ctrl.intRX = 1;
  ctrl.speed = SPEED;  // operate at the second highest data rate
  uart->ctl  = ctrl;

  Ud.nrx = 0;
  Ud.rx_hd = 0;
  Ud.rx_tl = 0;

  enableInterr();

  char c = 'a';
  do {
    if(proberx() > 0) {
      c = Getc();
      to_stdout( (int)c );
    }
    
  } while(c != EOT);


}
