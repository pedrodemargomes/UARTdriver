	.file	1 "trab_uart_fib.c"
	.section .mdebug.abi32
	.previous
	.nan	legacy
	.module	fp=32
	.module	nooddspreg
	.text
	.align	2
	.globl	proberx
	.set	nomips16
	.set	nomicromips
	.ent	proberx
	.type	proberx, @function
proberx:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	lui	$2,%hi(Ud)
	addiu	$2,$2,%lo(Ud)
	lw	$2,48($2)
	j	$31
	nop

	.set	macro
	.set	reorder
	.end	proberx
	.size	proberx, .-proberx
	.align	2
	.globl	Getc
	.set	nomips16
	.set	nomicromips
	.ent	Getc
	.type	Getc, @function
Getc:
	.frame	$sp,24,$31		# vars= 0, regs= 2/0, args= 16, gp= 0
	.mask	0x80010000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-24
	sw	$31,20($sp)
	jal	disableInterr
	sw	$16,16($sp)

	lui	$2,%hi(Ud)
	addiu	$4,$2,%lo(Ud)
	lw	$3,48($4)
	nop
	addiu	$3,$3,-1
	sw	$3,48($4)
	lw	$3,%lo(Ud)($2)
	nop
	addu	$3,$3,$4
	lbu	$16,8($3)
	nop
	sll	$16,$16,24
	sra	$16,$16,24
	lw	$2,%lo(Ud)($2)
	nop
	addiu	$3,$2,1
	li	$2,-2147483648			# 0xffffffff80000000
	addiu	$2,$2,15
	and	$2,$3,$2
	bgez	$2,$L5
	lui	$3,%hi(Ud)

	addiu	$2,$2,-1
	li	$3,-16			# 0xfffffffffffffff0
	or	$2,$2,$3
	addiu	$2,$2,1
	lui	$3,%hi(Ud)
$L5:
	sw	$2,%lo(Ud)($3)
	jal	enableInterr
	nop

	move	$2,$16
	lw	$31,20($sp)
	lw	$16,16($sp)
	j	$31
	addiu	$sp,$sp,24

	.set	macro
	.set	reorder
	.end	Getc
	.size	Getc, .-Getc
	.align	2
	.globl	probetx
	.set	nomips16
	.set	nomicromips
	.ent	probetx
	.type	probetx, @function
probetx:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	lui	$2,%hi(Ud)
	addiu	$2,$2,%lo(Ud)
	lw	$2,52($2)
	j	$31
	nop

	.set	macro
	.set	reorder
	.end	probetx
	.size	probetx, .-probetx
	.align	2
	.globl	Putc
	.set	nomips16
	.set	nomicromips
	.ent	Putc
	.type	Putc, @function
Putc:
	.frame	$sp,24,$31		# vars= 0, regs= 2/0, args= 16, gp= 0
	.mask	0x80010000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-24
	sw	$31,20($sp)
	sw	$16,16($sp)
	sll	$16,$4,24
	jal	disableInterr
	sra	$16,$16,24

	lui	$2,%hi(Ud)
	addiu	$2,$2,%lo(Ud)
	lw	$3,52($2)
	nop
	addiu	$3,$3,-1
	sw	$3,52($2)
	lw	$3,28($2)
	nop
	addu	$3,$3,$2
	sb	$16,32($3)
	lw	$2,28($2)
	nop
	addiu	$3,$2,1
	li	$2,-2147483648			# 0xffffffff80000000
	addiu	$2,$2,15
	and	$2,$3,$2
	bgez	$2,$L11
	lui	$3,%hi(Ud)

	addiu	$2,$2,-1
	li	$3,-16			# 0xfffffffffffffff0
	or	$2,$2,$3
	addiu	$2,$2,1
	lui	$3,%hi(Ud)
$L11:
	addiu	$3,$3,%lo(Ud)
	sw	$2,28($3)
	lui	$2,%hi(tx_has_started)
	lw	$2,%lo(tx_has_started)($2)
	nop
	bne	$2,$0,$L9
	nop

	lw	$3,52($3)
	li	$2,14			# 0xe
	bne	$3,$2,$L9
	li	$2,1006632960			# 0x3c000000

	lw	$3,232($2)
	nop
	ori	$3,$3,0x40
	sw	$3,232($2)
$L9:
	jal	enableInterr
	nop

	lw	$31,20($sp)
	lw	$16,16($sp)
	j	$31
	addiu	$sp,$sp,24

	.set	macro
	.set	reorder
	.end	Putc
	.size	Putc, .-Putc
	.align	2
	.globl	ioctl
	.set	nomips16
	.set	nomicromips
	.ent	ioctl
	.type	ioctl, @function
ioctl:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	li	$2,1006632960			# 0x3c000000
	sw	$4,224($2)
	j	$31
	nop

	.set	macro
	.set	reorder
	.end	ioctl
	.size	ioctl, .-ioctl
	.align	2
	.globl	iostat
	.set	nomips16
	.set	nomicromips
	.ent	iostat
	.type	iostat, @function
iostat:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	move	$2,$4
	li	$3,1006632960			# 0x3c000000
	addiu	$3,$3,224
	lw	$3,4($3)
	j	$31
	sw	$3,0($4)

	.set	macro
	.set	reorder
	.end	iostat
	.size	iostat, .-iostat
	.align	2
	.globl	fibo
	.set	nomips16
	.set	nomicromips
	.ent	fibo
	.type	fibo, @function
fibo:
	.frame	$sp,0,$31		# vars= 0, regs= 0/0, args= 0, gp= 0
	.mask	0x00000000,0
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	sll	$4,$4,2
	lui	$2,%hi(dat)
	addiu	$2,$2,%lo(dat)
	addu	$4,$4,$2
	lw	$2,0($4)
	j	$31
	nop

	.set	macro
	.set	reorder
	.end	fibo
	.size	fibo, .-fibo
	.align	2
	.globl	main
	.set	nomips16
	.set	nomicromips
	.ent	main
	.type	main, @function
main:
	.frame	$sp,1360,$31		# vars= 1304, regs= 10/0, args= 16, gp= 0
	.mask	0xc0ff0000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-1360
	sw	$31,1356($sp)
	sw	$fp,1352($sp)
	sw	$23,1348($sp)
	sw	$22,1344($sp)
	sw	$21,1340($sp)
	sw	$20,1336($sp)
	sw	$19,1332($sp)
	sw	$18,1328($sp)
	sw	$17,1324($sp)
	sw	$16,1320($sp)
	li	$2,1006632960			# 0x3c000000
	li	$3,4			# 0x4
	sw	$3,224($2)
	li	$3,156			# 0x9c
	sw	$3,224($2)
	lui	$3,%hi(Ud)
	addiu	$2,$3,%lo(Ud)
	li	$4,16			# 0x10
	sw	$4,52($2)
	sw	$0,24($2)
	sw	$0,28($2)
	lui	$4,%hi(tx_has_started)
	sw	$0,%lo(tx_has_started)($4)
	sw	$0,48($2)
	sw	$0,%lo(Ud)($3)
	sw	$0,4($2)
	jal	enableInterr
	move	$17,$0

	move	$16,$0
	move	$20,$0
	move	$18,$0
	lui	$19,%hi(Ud)
	li	$21,-2147483648			# 0xffffffff80000000
	addiu	$23,$21,255
	lui	$2,%hi(dat)
	addiu	$2,$2,%lo(dat)
	b	$L16
	sw	$2,1312($sp)

$L51:
	move	$16,$17
$L16:
	li	$22,1			# 0x1
$L66:
	b	$L17
	li	$fp,10			# 0xa

$L21:
	jal	Getc
	nop

	addiu	$3,$sp,16
	addu	$3,$3,$20
	addiu	$20,$20,1
	and	$20,$20,$23
	bgez	$20,$L18
	sb	$2,0($3)

	addiu	$20,$20,-1
	li	$3,-256			# 0xffffffffffffff00
	or	$20,$20,$3
	addiu	$20,$20,1
$L18:
	beq	$2,$fp,$L19
	sll	$22,$22,4

	li	$3,4			# 0x4
	beq	$2,$3,$L20
	addu	$2,$sp,$17

$L17:
	addiu	$2,$19,%lo(Ud)
	lw	$2,48($2)
	nop
	bgtz	$2,$L21
	addiu	$2,$19,%lo(Ud)

$L69:
	lw	$2,48($2)
	nop
	blez	$2,$L69
	addiu	$2,$19,%lo(Ud)

	b	$L21
	nop

$L27:
	addiu	$6,$21,1023
	li	$7,-1024			# 0xfffffffffffffc00
	addiu	$2,$sp,16
$L70:
	addu	$2,$2,$18
	lb	$2,0($2)
	nop
	slt	$5,$2,58
	beq	$5,$0,$L23
	nop

	b	$L24
	addiu	$2,$2,-48

$L23:
	addiu	$2,$2,-87
$L24:
	mult	$3,$2
	mflo	$2
	addiu	$18,$18,1
	and	$18,$18,$6
	bgez	$18,$L25
	addu	$4,$2,$4

	addiu	$18,$18,-1
	or	$18,$18,$7
	addiu	$18,$18,1
$L25:
	bgez	$3,$L26
	move	$2,$3

	addiu	$2,$3,15
$L26:
	sra	$3,$2,4
	bne	$3,$0,$L70
	addiu	$2,$sp,16

	sll	$4,$4,2
$L68:
	lw	$2,1312($sp)
	nop
	addu	$4,$4,$2
	lw	$4,0($4)
	li	$2,10			# 0xa
	beq	$4,$0,$L50
	sb	$2,1296($sp)

	addiu	$6,$sp,1296
	li	$3,1			# 0x1
	addiu	$7,$21,15
	li	$8,-16			# 0xfffffffffffffff0
$L33:
	and	$2,$4,$7
	bgez	$2,$L71
	sll	$5,$2,24

	addiu	$2,$2,-1
	or	$2,$2,$8
	addiu	$2,$2,1
	sll	$5,$2,24
$L71:
	sra	$5,$5,24
	slt	$5,$5,10
	beq	$5,$0,$L30
	nop

	addiu	$2,$2,48
	b	$L31
	andi	$2,$2,0x00ff

$L30:
	andi	$2,$2,0xf
	addiu	$2,$2,87
$L31:
	sb	$2,1($6)
	addiu	$3,$3,1
	bgez	$4,$L32
	move	$2,$4

	addiu	$2,$4,15
$L32:
	sra	$4,$2,4
	bne	$4,$0,$L33
	addiu	$6,$6,1

	slt	$2,$3,9
	bne	$2,$0,$L28
	nop

	b	$L65
	addiu	$2,$sp,1296

$L50:
	li	$3,1			# 0x1
$L28:
	addiu	$2,$sp,1296
	addu	$2,$2,$3
	li	$5,48			# 0x30
$L35:
	sb	$5,0($2)
	addiu	$3,$3,1
	slt	$4,$3,9
	bne	$4,$0,$L35
	addiu	$2,$2,1

	addiu	$2,$sp,1296
$L65:
	addiu	$6,$sp,1287
	addiu	$5,$21,1023
	li	$7,-1024			# 0xfffffffffffffc00
$L37:
	addiu	$3,$sp,16
	addu	$3,$3,$17
	lbu	$4,8($2)
	addiu	$17,$17,1
	and	$17,$17,$5
	bgez	$17,$L36
	sb	$4,256($3)

	addiu	$17,$17,-1
	or	$17,$17,$7
	addiu	$17,$17,1
$L36:
	addiu	$2,$2,-1
	bne	$2,$6,$L37
	nop

	beq	$16,$17,$L51
	addiu	$2,$19,%lo(Ud)

	lw	$2,52($2)
	nop
	blez	$2,$L66
	li	$22,1			# 0x1

	addiu	$22,$21,1023
	li	$fp,-1024			# 0xfffffffffffffc00
	addiu	$2,$sp,16
$L72:
	addu	$2,$2,$16
	lb	$4,256($2)
	jal	Putc
	addiu	$16,$16,1

	and	$16,$16,$22
	bgez	$16,$L39
	nop

	addiu	$16,$16,-1
	or	$16,$16,$fp
	addiu	$16,$16,1
$L39:
	beq	$17,$16,$L16
	addiu	$2,$19,%lo(Ud)

	lw	$2,52($2)
	nop
	bgtz	$2,$L72
	addiu	$2,$sp,16

	b	$L66
	li	$22,1			# 0x1

$L41:
$L67:
	lw	$2,52($2)
	nop
	blez	$2,$L41
	addiu	$2,$18,%lo(Ud)

$L44:
	addiu	$2,$sp,16
	addu	$2,$2,$16
	lb	$4,256($2)
	jal	Putc
	addiu	$16,$16,1

	and	$16,$16,$19
	bgez	$16,$L46
	nop

	addiu	$16,$16,-1
	or	$16,$16,$20
	addiu	$16,$16,1
$L46:
	beq	$16,$17,$L43
	addiu	$2,$18,%lo(Ud)

	lw	$2,52($2)
	nop
	bgtz	$2,$L44
	addiu	$2,$18,%lo(Ud)

	b	$L67
	nop

$L43:
	move	$5,$0
	jal	startCounter
	li	$4,20000			# 0x4e20

	li	$16,1073676288			# 0x3fff0000
	ori	$16,$16,0xffff
$L45:
	jal	readCounter
	nop

	and	$2,$2,$16
	slt	$3,$2,20000
	bne	$3,$0,$L45
	nop

	b	$L63
	nop

$L20:
	li	$3,4			# 0x4
	sb	$3,272($2)
	addiu	$17,$17,1
	li	$2,1024			# 0x400
	bne	$2,$0,1f
	div	$0,$17,$2
	break	7
1:
	mfhi	$17
	lui	$18,%hi(Ud)
	li	$19,-2147483648			# 0xffffffff80000000
	addiu	$19,$19,1023
	b	$L46
	li	$20,-1024			# 0xfffffffffffffc00

$L19:
	addiu	$20,$20,-1
	and	$20,$20,$23
	bgez	$20,$L47
	li	$2,-256			# 0xffffffffffffff00

	addiu	$20,$20,-1
	or	$20,$20,$2
	addiu	$20,$20,1
$L47:
	bgez	$22,$L48
	move	$3,$22

	addiu	$3,$22,255
$L48:
	sra	$3,$3,8
	bne	$3,$0,$L27
	move	$4,$0

	b	$L68
	sll	$4,$4,2

$L63:
	lw	$31,1356($sp)
	lw	$fp,1352($sp)
	lw	$23,1348($sp)
	lw	$22,1344($sp)
	lw	$21,1340($sp)
	lw	$20,1336($sp)
	lw	$19,1332($sp)
	lw	$18,1328($sp)
	lw	$17,1324($sp)
	lw	$16,1320($sp)
	j	$31
	addiu	$sp,$sp,1360

	.set	macro
	.set	reorder
	.end	main
	.size	main, .-main
	.rdata
	.align	2
	.type	dat, @object
	.size	dat, 400
dat:
	.word	1
	.word	1
	.word	2
	.word	3
	.word	5
	.word	8
	.word	13
	.word	21
	.word	34
	.word	55
	.word	89
	.word	144
	.word	233
	.word	377
	.word	610
	.word	987
	.word	1597
	.word	2584
	.word	4181
	.word	6765
	.word	10946
	.word	17711
	.word	28657
	.word	46368
	.word	75025
	.word	121393
	.word	196418
	.word	317811
	.word	514229
	.word	832040
	.word	1346269
	.word	2178309
	.word	3524578
	.word	5702887
	.word	9227465
	.word	14930352
	.word	24157817
	.word	39088169
	.word	63245986
	.word	102334155
	.word	165580141
	.word	267914296
	.word	433494437
	.word	701408733
	.word	1134903170
	.word	1836311903
	.word	-1323752223
	.word	512559680
	.word	-811192543
	.word	-298632863
	.word	-1109825406
	.word	-1408458269
	.word	1776683621
	.word	368225352
	.word	2144908973
	.word	-1781832971
	.word	363076002
	.word	-1418756969
	.word	-1055680967
	.word	1820529360
	.word	764848393
	.word	-1709589543
	.word	-944741150
	.word	1640636603
	.word	695895453
	.word	-1958435240
	.word	-1262539787
	.word	1073992269
	.word	-188547518
	.word	885444751
	.word	696897233
	.word	1582341984
	.word	-2015728079
	.word	-433386095
	.word	1845853122
	.word	1412467027
	.word	-1036647147
	.word	375819880
	.word	-660827267
	.word	-285007387
	.word	-945834654
	.word	-1230842041
	.word	2118290601
	.word	887448560
	.word	-1289228135
	.word	-401779575
	.word	-1691007710
	.word	-2092787285
	.word	511172301
	.word	-1581614984
	.word	-1070442683
	.word	1642909629
	.word	572466946
	.word	-2079590721
	.word	-1507123775
	.word	708252800
	.word	-798870975
	.word	-90618175
	.word	-889489150
	.word	-980107325
	.ident	"GCC: (GNU) 5.1.0"
